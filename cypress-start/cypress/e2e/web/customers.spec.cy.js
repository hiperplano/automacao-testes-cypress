import { faker } from '@faker-js/faker'

describe('Usuários devem visualizar a lita de clientes', () => {

  before(() => {
    cy.auth_bypass_api();
  });

  beforeEach(() => {
    cy.intercept("**/api/v1/customers/all", { fixture: 'customers.json' })
  })

  it('através do form da aplicação com sucesso', () => {
    cy.list_customers();
  })

})

describe('Usuários devem realizar o cadastro de novos clientes', () => {

  before(() => {
    cy.auth_bypass_api();
  });

  it('atraves do form da aplicacao com sucesso', () => {
    cy.new_customer(faker.name.fullName(),
      faker.internet.email(),
      faker.company.name(),
      faker.random.numeric(6),
      faker.address.cityName(),
      faker.address.state(),
      faker.address.streetAddress(),
      faker.address.country(),
      faker.address.zipCode(),
      faker.phone.number())
  })
})