import * as urls from './urls.js'
import login from '../selectors/login.sel.cy.js'
import users from '../selectors/users.sel.cy.js'
import customers from '../selectors/customers.sel.cy.js'

require('cypress-xpath')

Cypress.Commands.add('login', (username, password) => {
    cy.visit(urls.web.app)
    cy.get(login.username).type(username)
    cy.get(login.password).type(password)
    cy.get(login.loginButton).click()
    cy.wait('@authRequest').then(({response}) => {
        expect(response.statusCode).to.eq(200)
        expect(response.body.token).to.exist
    })
    cy.contains('h1', 'Dashboard')
})

Cypress.Commands.add('new_user', (name, username, password, roles) => {
    setAuthCookies()
    cy.visit(urls.web.users.edit, { onBeforeLoad(win) { setAuthSessionStorage(win) } })
    cy.get(users.name).click({ force: true }).type(name)
    cy.get(users.username).type(username)
    cy.get(users.password).type(password)
    cy.get(users.passwordConfirmation).type(password)
    cy.get(users.roles).type(roles)
    cy.get(users.submit).click()
    cy.get(users.panel_success).should('be.visible')
})

function setAuthCookies() {
    cy.setCookie("user.id", String(Cypress.env("auth.user.id")))
    cy.setCookie("token", Cypress.env("auth.token").replace(" ", "%20"))
    cy.request(urls.web.authorize)
}

function setAuthSessionStorage(win) {
    win.sessionStorage.setItem("user.id", Cypress.env("auth.user.id"))
    win.sessionStorage.setItem("token", Cypress.env("auth.token"))
}

Cypress.Commands.add('new_customer', (name, email, company, salary, city, state, address, country, zipCode, phoneNumber) => {
    setAuthCookies()
    cy.visit(urls.web.customers.edit, { onBeforeLoad(win) { setAuthSessionStorage(win) } })
    cy.xpath(customers["form-edit"].name).click({ force: true }).type(name)
    cy.xpath(customers["form-edit"].email).click({ force: true }).type(email)
    cy.xpath(customers["form-edit"].company).click({ force: true }).type(company)
    cy.xpath(customers["form-edit"].salary).click({ force: true }).type(salary)
    cy.xpath(customers["form-edit"].city).click({ force: true }).type(city)
    cy.xpath(customers["form-edit"].state).click({ force: true }).type(state)
    cy.xpath(customers["form-edit"].address).click({ force: true }).type(address)
    cy.xpath(customers["form-edit"].country).select(country)
    cy.xpath(customers["form-edit"].zipCode).click({ force: true }).type(zipCode)
    cy.xpath(customers["form-edit"].phoneNumber).click({ force: true }).type(phoneNumber)
    cy.xpath(customers["form-edit"].submit).click({ force: true })
    cy.xpath(customers["form-edit"].panel_success).should('be.visible')
})

Cypress.Commands.add('list_customers', () => {
    setAuthCookies()
    cy.visit(urls.web.customers.list, { onBeforeLoad(win) { setAuthSessionStorage(win) } })
    cy.xpath(customers["form-list"].panel_success).should('be.visible')
})