export const web = {
    app: "https://curso-automacao-web-app.herokuapp.com",
    authorize: "https://curso-automacao-web-app.herokuapp.com/authorize",
    login: "https://curso-automacao-web-app.herokuapp.com/login",
    users:
    {
        list: "https://curso-automacao-web-app.herokuapp.com/users-list",
        edit: "https://curso-automacao-web-app.herokuapp.com/users-edit"
    },
    customers:
    {
        list: "https://curso-automacao-web-app.herokuapp.com/customers-list",
        edit: "https://curso-automacao-web-app.herokuapp.com/customers-edit"
    }
}

export const api = {
    users: {
        url: "https://curso-automacao-user-service.herokuapp.com",
        auth: "https://curso-automacao-user-service.herokuapp.com/auth",
        find_by_username: "https://curso-automacao-user-service.herokuapp.com/api/v1/users/find-by/user-name/"
    },
    customers: {
        url: "https://curso-automacao-customer-service.herokuapp.com",
        all: "https://curso-automacao-customer-service.herokuapp.com/api/v1/customers/all"
    }
}